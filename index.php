<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Palgakalkulaator</title>
</head>
<body>
    <form method="post">
        <input type="text" name="bruto" placeholder="Bruto palk" />
        <input type="submit" value="Arvuta"/>
    </form>
    <?php

    $tulumaksuvabaMiinimum = 500;

    if (isset($_POST['bruto'])) {
        $brutoPalk = $_POST['bruto'];
        $kogumisPension = $brutoPalk * 0.02;
        $tootusKindlustus = $brutoPalk * 0.016;

        $maksustatavTulu = $brutoPalk - $tulumaksuvabaMiinimum - $kogumisPension - $tootusKindlustus;

        $tulumaks = 0;
        if ($maksustatavTulu > 0) {
            $tulumaks = $maksustatavTulu * 0.2;
        }

        $netoPalk = $brutoPalk - $tulumaks - $kogumisPension - $tootusKindlustus;

        $sotsMaks = $brutoPalk * 0.33;
        $tooandjaTootusKindlustus = $brutoPalk * 0.008;

        $tooandjaKulu = $brutoPalk + $sotsMaks + $tooandjaTootusKindlustus;
    }

    ?>

    <?php if (isset($brutoPalk)) : ?>
        <table>
            <tr>
                <td>Tööandja kulu</td>
                <td><?php echo $tooandjaKulu ?>€</td>
            </tr>
            <tr>
                <td>Sotsiaalmaks (33%)</td>
                <td><?php echo $sotsMaks ?>€</td>
            </tr>
            <tr>
                <td>Töötuskindlustus (tööandja) (0.8%)</td>
                <td><?php echo $tooandjaTootusKindlustus ?>€</td>
            </tr>
            <tr>
                <td>Bruto palk</td>
                <td><?php echo $brutoPalk ?>€</td>
            </tr>
            <tr>
                <td>Tulumaks (20%)</td>
                <td><?php echo $tulumaks ?>€</td>
            </tr>
            <tr>
                <td>Kogumispension (2%)</td>
                <td><?php echo $kogumisPension ?>€</td>
            </tr>
            <tr>
                <td>Töötuskindlustus (töötaja) (1.6%)</td>
                <td><?php echo $tootusKindlustus ?>€</td>
            </tr>
            <tr>
                <td>Neto palk</td>
                <td><?php echo $netoPalk ?>€</td>
            </tr>
        </table>
    <?php endif; ?>
</body>
</html>